package tree

import "fmt"

type Node struct {
	X        Dominance
	Children []*Node
	Parent   *Node
}

func CreateTree() *Node {
	return &Node{nil, []*Node{}, nil}
}

func (n *Node) String() string {
	var parent Dominance = nil
	if n.Parent != nil {
		parent = n.Parent.X
	}
	return fmt.Sprintf("|X: %v, Children: %v, Parent: %v|", n.X, n.Children, parent)
}

func Add(x Dominance, root *Node) {

	for _, c := range root.Children {
		if add(x, c) {
			return
		}
	}

	xNode := createNode(x, root)
	root.Children = append(root.Children, xNode)
}

func add(x Dominance, n *Node) bool {
	if x.Dominates(n.X) {
		swap(x, n)
		return true
	}

	if n.X.Dominates(x) {
		for _, c := range n.Children {
			if add(x, c) {
				return true
			}
		}

		xNode := createNode(x, n)
		n.Children = append(n.Children, xNode)
		return true
	}

	return false
}

func swap(d Dominance, n *Node) {
	p := n.Parent
	dNode := createNode(d, p)
	p.Children = append(p.Children, dNode)

	n.Parent = dNode

	for i, c := range p.Children {
		if c == n {
			removeNode(p, i)
			break
		}
	}

	dNode.Children = []*Node{n}
}

func removeNode(n *Node, i int) {
	a := n.Children
	copy(a[i:], a[i+1:])
	a[len(a)-1] = nil
	n.Children = a[:len(a)-1]
}

func createNode(x Dominance, p *Node) *Node {
	return &Node{x, []*Node{}, p}
}
