package tree

type Dominance interface {
	Dominates(x Dominance) bool
}
