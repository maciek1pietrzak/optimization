package tree

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestAddingNewNode(t *testing.T) {
	// given
	x := CreateTree()

	// when
	Add(tDominance{5}, x)

	// then
	node := expectedTreeForNewNode()
	assert.Equal(t, &node, x)
}

func TestAddingDominatedNodeToExistingTree(t *testing.T) {
	// given
	x := CreateTree()
	Add(tDominance{5}, x)

	// when
	Add(tDominance{3}, x)

	// then
	node := expectedTreeForDominatedNode()

	assert.Equal(t, &node, x)
}

func TestAddingDominatingNodeToExistingTree(t *testing.T) {
	// given
	x := CreateTree()
	Add(tDominance{5}, x)

	// when
	Add(tDominance{6}, x)

	// then
	node := expectedTreeForDominatingNode()

	assert.Equal(t, &node, x)
}

func TestAddingSameNodeToExistingTree(t *testing.T) {
	// given
	x := CreateTree()
	Add(tDominance{5}, x)

	// when
	Add(tDominance{5}, x)

	// then
	node := expectedTreeForSameNodes()

	assert.Equal(t, &node, x)
}

func expectedTreeForDominatingNode() Node {
	i := createNode(tDominance{6}, nil)
	r := createNode(tDominance{5}, i)
	i.Children = append(i.Children, r)
	node := Node{nil, []*Node{i}, nil}
	i.Parent = &node

	return node
}

func expectedTreeForSameNodes() Node {
	a := createNode(tDominance{5}, nil)
	b := createNode(tDominance{5}, nil)
	node := Node{nil, []*Node{a, b}, nil}
	a.Parent = &node
	b.Parent = &node

	return node
}

func expectedTreeForDominatedNode() Node {
	r := createNode(tDominance{5}, nil)
	i := createNode(tDominance{3}, r)
	r.Children = append(r.Children, i)
	node := Node{nil, []*Node{r}, nil}
	r.Parent = &node

	return node
}

func expectedTreeForNewNode() Node {
	i := createNode(tDominance{5}, nil)
	node := Node{nil, []*Node{i}, nil}
	i.Parent = &node
	return node
}

type tDominance struct {
	a int
}

func (d tDominance) Dominates(x Dominance) bool {
	xD, ok := x.(tDominance)
	if !ok {
		panic("wrong type")
	}

	return d.a > xD.a
}
