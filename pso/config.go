package main

type PsoConfig struct {
	c1, c2, c3 float64
	xDom       Domain
	it         int
	population int
	functions  []Function
	wages      []float64
}

type Domain struct {
	min, max float64
}

func (c PsoConfig) getXMin() float64 {
	return c.xDom.min
}

func (c PsoConfig) getXMax() float64 {
	return c.xDom.max
}
