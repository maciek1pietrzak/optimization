package main

import (
	rndGen "bitbucket.org/maciek1pietrzak/utils"
	"fmt"
)

type particle struct {
	v, x, y, yBest, pBest float64
}

func Pso(c PsoConfig) (float64, float64) {

	ctx := initContext(c)
	ctx.initializeSwarm()

	for i := 0; i < c.it; i++ {
		ctx.reduceSpeed()
		ctx.swarmIteration()
	}

	return ctx.xBest, ctx.yBest
}

func (ctx *context) reduceSpeed() {
	ctx.cfg.c1 *= 0.95
}

func (ctx *context) initializeSwarm() {
	population := ctx.cfg.population
	swarm := make([]particle, population)

	xMin := ctx.getMinDomain()
	xMax := ctx.getMaxDomain()

	xBest := rndGen.RandomFloat(xMin, xMax)
	yBest := ctx.valueFor(xBest)
	for i := 0; i < population; i++ {
		s := new(particle)
		s.v = 0
		// @todo need random with uniform distribution
		s.x = rndGen.RandomFloat(xMin, xMax)
		s.pBest = s.x
		s.y = ctx.valueFor(s.x)
		s.yBest = s.y

		if yBest > s.yBest {
			yBest = s.yBest
			xBest = s.x
		}
		swarm[i] = *s
	}

	ctx.swarm = swarm
	ctx.xBest = xBest
	ctx.yBest = yBest
}

func (ctx *context) swarmIteration() {
	for i, p := range ctx.swarm {
		v := ctx.countVelocity(p)
		p.x += v
		p.v = v
		ctx.protectBoundaries(&p)
		y := ctx.valueFor(p.x)
		p.y = y
		if p.yBest > y {
			p.yBest = y
			p.pBest = p.x

			if ctx.yBest > y {
				ctx.yBest = y
				ctx.xBest = p.x
			}
		}

		ctx.swarm[i] = p
	}
	fmt.Printf("Min (%f) at %f\n", ctx.yBest, ctx.xBest)
}

func (ctx *context) valueFor(x float64) float64 {
	fs := ctx.cfg.functions
	ws := ctx.cfg.wages

	y := 0.0
	for i := range fs {
		y += fs[i].count(x) * ws[i]
	}

	return y
}

func (ctx *context) countVelocity(s particle) float64 {
	r1, r2, r3 := rndGen.Float64(), rndGen.Float64(), rndGen.Float64()
	c1, c2, c3 := ctx.vFactors()
	f := c1*r1*s.v + c2*r2*(s.pBest-s.x) + c3*r3*(ctx.xBest-s.x)
	return f
}

func (ctx *context) protectBoundaries(s* particle) {
	xMax := ctx.getMaxDomain()
	xMin := ctx.getMinDomain()

	if s.x < xMin {
		s.x = xMin
	} else if s.x > xMax {
		s.x = xMax
	}
}
