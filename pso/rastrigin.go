package main

import "math"

type rastrigin struct {
}

func (f rastrigin) count(x float64) float64 {
	return 5*10*x*x - 10*math.Cos(2*math.Pi*x)
}
