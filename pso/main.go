package main

import "fmt"

func main() {

	conf := PsoConfig{
		1.0, 0.2, 0.65,
		Domain{-5.12, 5.12},
		100,
		10,
		[]Function{rastrigin{}},
		//[]Function{pow{}},
		[]float64{1},
	}

	xBest, yBest := Pso(conf)

	fmt.Printf("Min (%f) at %f\n", yBest, xBest)
}
