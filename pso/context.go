package main


type context struct {
	cfg   PsoConfig
	swarm []particle
	xBest float64
	yBest float64
}

func initContext(cfx PsoConfig) *context {
	return &context{
		cfg:cfx,
		swarm:nil,
		xBest:0,
		yBest:0,
	}
}

func (ctx context) vFactors() (float64, float64, float64) {
	return ctx.cfg.c1, ctx.cfg.c2, ctx.cfg.c3
}

func (ctx context) getMinDomain() float64 {
	return ctx.cfg.xDom.min
}

func (ctx context) getMaxDomain() float64 {
	return ctx.cfg.xDom.max
}