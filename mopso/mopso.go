package mopso

import (
	rndGen "bitbucket.org/maciek1pietrzak/utils"
	"bitbucket.org/maciek1pietrzak/optimization/tree"
	"golang.org/x/exp/rand"
)

func (ctx *context) initSwarm() {
	population := ctx.population()
	swarm := []Particle{}

	for i := 0; i < population; i++ {

		fIt := []float64{}
		xIt := []float64{}
		vIt := []float64{}

		for dim := 0; dim < ctx.dimentionLen(); dim++ {
			xMin, xMax := ctx.domainFor(dim)
			xIt = append(xIt, rndGen.RandomFloat(xMin, xMax))
			vIt = append(vIt, rndGen.Float64())
		}

		for dim := 0; dim < ctx.dimentionLen(); dim++ {
			f := ctx.valueFor(xIt, dim)
			fIt = append(fIt, f)
		}

		p := Particle{
			fIt:   fIt,
			xIt:   xIt,
			vIt:   vIt,
			pBest: nil,
		}
		p.pBest = p.copy()
		swarm = append(swarm, p)
	}

	ctx.particles = swarm
}

func Mopso(c MopsoConfig) []Particle { // @todo make Particle more representative?
	ctx := initContext(c)
	ctx.initSwarm()
	ctx.archive = getNotDominated(createTree(ctx))

	for i := 0; i < ctx.it(); i++ {
		for i := range ctx.particles {
			rndBest := getRandomFromArchive(ctx.archive)
			p := ctx.particles[i]

			for it := range p.vIt {
				r1, r2, r3 := rndGen.Float64(), rndGen.Float64(), rndGen.Float64()
				c1, c2, c3 := ctx.vFactors()
				vIt := c1*r1*p.vIt[it] + c2*r2*(p.xBest(it)-p.xIt[it]) + c3*r3*(rndBest.xIt[it]-p.xIt[it])

				p.xIt[it] = p.xIt[it] + vIt
				p.vIt[it] = vIt
			}

			for dim := range p.fIt {
				f := ctx.valueFor(p.xIt, dim)
				p.fIt[dim] = f
			}

			if p.Dominates(*p.pBest) {
				p.pBest = p.copy()
			}
		}

		tmpArchive := make([]Particle, len(ctx.archive))
		copy(tmpArchive, ctx.archive)
		root := tree.CreateTree()
		for _, p := range ctx.particles {
			tree.Add(p, root)
		}

		newRoot := createTree(ctx)
		for _, nd := range tmpArchive {
			tree.Add(nd, newRoot)
		}
		ctx.archive = getNotDominated(newRoot) // @todo może zmniejsz liczebność?
	}

	return ctx.archive
}

func createTree(ctx *context) *tree.Node {
	root := tree.CreateTree()
	for i := 0; i < len(ctx.particles); i++ {
		p := ctx.particles[i]
		tree.Add(p, root)
	}
	return root
}

func getRandomFromArchive(particles []Particle) Particle {
	idx := rand.Intn(len(particles))
	return particles[idx]
}

func getNotDominated(root *tree.Node) []Particle {
	notDominated := make([]Particle, 0)
	for _, c := range root.Children {
		p := particleFrom(c.X)
		notDominated = append(notDominated, p)
	}
	return notDominated
}
