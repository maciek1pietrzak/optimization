package mopso

type MopsoConfig struct {
	c1, c2, c3 float64
	population int
	it         int
	functions  []Function

	xDom []Domain
	dim  int
}

type Function interface {
	count([]float64) float64
}

type Domain struct {
	min, max float64
}
