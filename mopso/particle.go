package mopso

import (
	"bitbucket.org/maciek1pietrzak/optimization/tree"
	"log"
)

type Particle struct {
	vIt []float64
	xIt []float64
	fIt []float64

	pBest *Particle
}

func (p Particle) Dominates(x tree.Dominance) bool {
	px := particleFrom(x)
	atLeastOne := false
	for it := 0; it < len(px.fIt); it++ {
		if p.fIt[it] > px.fIt[it] {
			return false
		} else if p.fIt[it] < px.fIt[it] {
			atLeastOne = true
		}
	}

	return atLeastOne
}

func (p Particle) xBest(it int) float64 {
	return p.pBest.xIt[it]
}

func (p Particle) copy() *Particle {
	dim := len(p.xIt)
	vIt := make([]float64, dim)
	xIt := make([]float64, dim)
	fIt := make([]float64, dim)

	copy(vIt, p.vIt)
	copy(xIt, p.xIt)
	copy(fIt, p.fIt)
	return &Particle{vIt, xIt, fIt, nil}
}

func particleFrom(x tree.Dominance) Particle {
	p, ok := x.(Particle)
	if !ok {
		log.Panicf("cannot cast %v to %v", x, Particle{})
	}
	return p
}
