package mopso

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestParticleMapping(t *testing.T) {
	// given
	x := &Particle{fIt: []float64{1}, xIt: []float64{1}, vIt: []float64{1}, pBest: nil}

	// when
	from := particleFrom(*x)

	// then
	assert.Equal(t, from, *x)
}
