package mopso

import (
	"testing"
	"bitbucket.org/maciek1pietrzak/optimization/tree"
	"github.com/stretchr/testify/assert"
)

func TestCreatingTreeFromContext(t *testing.T) {
	// given
	someConfig := MopsoConfig{}
	ctx := initContext(someConfig)
	ctx.particles = []Particle{
		makeParticle(2, 10),
		makeParticle(0, 6),
		makeParticle(1, 5),
	}

	// when
	root := createTree(ctx)

	// then
	assert.Equal(t, expectedTree(), root)
}

func expectedTree() *tree.Node {
	root := &tree.Node{nil, []*tree.Node{}, nil}

	p1 := &tree.Node{X: makeParticle(0, 6), Children: []*tree.Node{}, Parent: root}
	p2 := &tree.Node{X: makeParticle(1, 5), Children: []*tree.Node{}, Parent: root}
	p3 := &tree.Node{X: makeParticle(2, 10), Children: []*tree.Node{}, Parent: p1}
	p1.Children = []*tree.Node{p3}
	root.Children = []*tree.Node{p1, p2}
	return root
}
