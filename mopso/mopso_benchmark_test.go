package mopso

import (
	"testing"
	"math"
)

var pts []Particle

func BenchmarkMopso_2Dim(b *testing.B) {
	// given
	d := Domain{min: 0, max: 10}
	domains := []Domain{d, d}
	functions := []Function{p1{}, p1{}}

	config := MopsoConfig{dim: 2, population: 100, it: 100, c1: 1, c2: 1, c3: 1, xDom: domains, functions: functions}

	// when

	testFunction(b.N, config)
}

func BenchmarkMopso_5Dim(b *testing.B) {
	// given
	d := Domain{min: 0, max: 10}
	domains := []Domain{d, d}
	functions := []Function{p1{}, p1{}, p1{}, p1{}, p1{}}

	config := MopsoConfig{dim: 2, population: 100, it: 100, c1: 1, c2: 1, c3: 1, xDom: domains, functions: functions}

	// when

	testFunction(b.N, config)
}

func BenchmarkMopso_10Dim(b *testing.B) {
	// given
	d := Domain{min: 0, max: 10}
	domains := []Domain{d, d}
	functions := []Function{p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}}

	config := MopsoConfig{dim: 2, population: 100, it: 100, c1: 1, c2: 1, c3: 1, xDom: domains, functions: functions}

	// when

	testFunction(b.N, config)
}

func BenchmarkMopso_100Dim(b *testing.B) {
	// given
	d := Domain{min: 0, max: 10}
	domains := []Domain{
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
		d, d, d, d, d, d, d, d, d, d,
	}
	functions := []Function{
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
		p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{}, p1{},
	}

	config := MopsoConfig{dim: 2, population: 100, it: 100, c1: 1, c2: 1, c3: 1, xDom: domains, functions: functions}

	// when

	testFunction(b.N, config)
}

func testFunction(n int, config MopsoConfig) {
	var particles []Particle
	for i := 0; i < n; i++ {
		particles = Mopso(config)
	}
	pts = particles
}

type p1 struct{}

func (f p1) count(x []float64) float64 {
	return math.Sin(math.Pow(x[0], 2))
}
