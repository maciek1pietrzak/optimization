package mopso

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestDominatingRelation(t *testing.T) {
	// given
	x := Particle{fIt: []float64{0}}
	dominated := Particle{fIt: []float64{1}}

	// when
	isDominating := x.Dominates(dominated)

	// then
	assert.True(t, isDominating)
}

func TestNotDominatingRelation(t *testing.T) {
	// given
	x := Particle{fIt: []float64{10}}
	dominated := Particle{fIt: []float64{0}}

	// when
	isDominating := x.Dominates(dominated)

	// then
	assert.False(t, isDominating)
}

func TestEqualsRelation(t *testing.T) {
	// given
	x := Particle{fIt: []float64{0}}
	dominated := Particle{fIt: []float64{0}}

	// when
	isDominating := x.Dominates(dominated)

	// then
	assert.False(t, isDominating)
}

func TestDominatingRelationWithMultileDim(t *testing.T) {
	// given
	x := Particle{fIt: []float64{0, 1, 2}}
	dominated := Particle{fIt: []float64{0, 1, 10}}

	// when
	isDominating := x.Dominates(dominated)

	// then
	assert.True(t, isDominating)
}
