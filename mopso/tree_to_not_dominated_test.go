package mopso

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/maciek1pietrzak/optimization/tree"
)

func TestMappingTreeToNotDominated(t *testing.T) {
	// given
	root := tree.Node{nil, []*tree.Node{}, nil}
	root.Children = []*tree.Node{
		makeNode(1, &root),
		makeNode(2, &root),
		makeNodeWithChild(3, &root),
	}

	// when
	notDominated := getNotDominated(&root)

	// then
	assert.Equal(t, expectedNotDominated(), notDominated)
}

func expectedNotDominated() []Particle {
	return []Particle{
		{vIt: []float64{1}, xIt: []float64{1}, fIt: []float64{1}},
		{vIt: []float64{2}, xIt: []float64{2}, fIt: []float64{2}},
		{vIt: []float64{3}, xIt: []float64{3}, fIt: []float64{3}},
	}
}

func makeNodeWithChild(x float64, root *tree.Node) *tree.Node {
	p := &tree.Node{
		makeParticle(x),
		nil,
		root,
	}

	c := &tree.Node{
		makeParticle(x + 1),
		nil,
		p,
	}

	p.Children = []*tree.Node{c}

	return p
}

func makeNode(x float64, root *tree.Node) *tree.Node {
	return &tree.Node{
		makeParticle(x),
		nil,
		root,
	}
}

func makeParticle(x ... float64) Particle {
	return Particle{
		vIt: x,
		xIt: x,
		fIt: x,
	}
}
