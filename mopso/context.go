package mopso

type context struct {
	cfg MopsoConfig

	particles []Particle
	archive   []Particle
}

func (ctx context) it() int {
	return ctx.cfg.it
}

func (ctx context) vFactors() (float64, float64, float64) {
	return ctx.cfg.c1, ctx.cfg.c2, ctx.cfg.c3
}

func (ctx *context) valueFor(x []float64, it int) float64 {
	f := ctx.cfg.functions[it]

	return f.count(x)
}
func (ctx *context) population() int {
	return ctx.cfg.population
}

func initContext(cfg MopsoConfig) *context {
	return &context{cfg, []Particle{}, []Particle{}}
}

func (ctx *context) dimentionLen() int {
	return ctx.cfg.dim
}

func (ctx *context) domainFor(it int) (float64, float64) {
	domain := ctx.cfg.xDom[it]
	return domain.min, domain.max
}
