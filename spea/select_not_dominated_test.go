package spea

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestFindingNotDominated(t *testing.T) {
	population := []Individual{
		newInd([]float64{10, 15, 20}),
		newInd([]float64{11, 16, 22}),
		newInd([]float64{9, 14, 19}),
		newInd([]float64{10, 14, 18}),
	}

	notDominated := selectNotDominated(population)

	expected := []Individual{
		newInd([]float64{9, 14, 19}),
		newInd([]float64{10, 14, 18}),
	}
	assert.Equal(t, expected, notDominated)
}
