package spea

import (
	"testing"
	rndGen "bitbucket.org/maciek1pietrzak/utils"
	"math/rand"
	"time"
	"github.com/stretchr/testify/assert"
)

type fun1 struct{}
type fun2 struct{}

func (fun1) count(x [] float64) float64 {
	return x[0] + 1
}

func (fun2) count(x [] float64) float64 {
	return x[1] + 2
}

func TestCrossingIndividuals(t *testing.T) {
	// given
	rndGen.NewRandomGen(rand.NewSource(time.Unix(100, 100).UnixNano()))

	ds := []Domain{{0, 10}, {0, 10}}
	fs := []Function{fun1{}, fun2{}}

	i1, i2 := newIndividual([]float64{1, 2}, fs), newIndividual([]float64{3, 4}, fs)

	// when
	_, b := cross(i1, i2, ds, fs)

	// then
	assert.True(t, b)
}

func TestOutOfDomainCrossing(t *testing.T) {
	// given
	rndGen.NewRandomGen(rand.NewSource(time.Unix(100, 100).UnixNano()))

	ds := []Domain{{0, 3}, {0, 4}}
	fs := []Function{fun1{}, fun2{}}

	i1, i2 := newIndividual([]float64{1, 2}, fs), newIndividual([]float64{3, 4}, fs)

	// when
	_, b := cross(i1, i2, ds, fs)

	// then
	assert.False(t, b)
}
