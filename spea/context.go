package spea

type context struct {
	cfg SpeaConfig

	population []Individual
	archive    []Individual
}

func createContext(config SpeaConfig) *context {
	return &context{cfg: config}
}

func (ctx *context) getDomainForDim(it int) Domain {
	return ctx.cfg.domains[it]
}

func (ctx *context) getFunctionForDim(it int) Function {
	return ctx.cfg.functions[it]
}

func (ctx *context) getIt() int {
	return ctx.cfg.it
}

func (ctx *context) getCrossoverChance() float64 {
	return ctx.cfg.crossoverChange
}

func (ctx *context) getMutationChance() float64 {
	return ctx.cfg.mutationChance
}

func (ctx *context) getDomains() []Domain {
	return ctx.cfg.domains
}

func (ctx *context) getFunctions() []Function {
	return ctx.cfg.functions
}

func (ctx *context) getArchiveSize() int {
	return ctx.cfg.archSize
}

func (ctx *context) getPopulationSize() int {
	return ctx.cfg.popSize
}

func (ctx *context) getDimensionSize() int {
	return len(ctx.cfg.functions)
}

func (ctx *context) getMinForDim(d int) float64 {
	return ctx.getDomainForDim(d).minX
}

func (ctx *context) getMaxForDim(d int) float64 {
	return ctx.getDomainForDim(d).maxX
}