package spea

import (
	rndGen "bitbucket.org/maciek1pietrzak/utils"
)

type Individual struct {
	// @todo czy ta nazwa zostaje?
	xIt []float64
	fIt []float64

	fit float64
}

func Spea(c SpeaConfig) []Individual {
	ctx := createContext(c)
	ctx.initPopulation()

	for it := 0; it < ctx.getIt(); it++ {
		union := append(ctx.population, ctx.archive...)
		arch := selectNotDominated(union)
		ctx.archive = arch
		ctx.fitnessAssignment()
		clustering(ctx.archive, ctx.getArchiveSize())

		union = append(ctx.population, ctx.archive...)
		p := ctx.selection(union)
		ctx.crossover(p)
		ctx.mutation(p, it)

		ctx.population = p
	}

	return ctx.archive
}

func clustering(individuals []Individual, size int) {
	// @todo
}

func (ctx *context) initPopulation() {
	dimensionSize := ctx.getDimensionSize()
	var population []Individual
	for i := 0; i < ctx.getPopulationSize(); i++ {
		x := make([]float64, dimensionSize)
		y := make([]float64, dimensionSize)
		for it := 0; i < dimensionSize; it++ {
			d := ctx.getDomainForDim(it)
			xIt := rndGen.RandomFloat(d.minX, d.maxX)
			x[it] = xIt
		}
		for it := 0; i < dimensionSize; it++ {
			f := ctx.getFunctionForDim(it)
			yIt := f.count(x)
			y[it] = yIt
		}
		population = append(population, Individual{xIt: x, fIt: y})
	}

	ctx.population = population
}

func (ctx *context) fitnessAssignment() {
	for i, _ := range ctx.archive {
		p := ctx.archive[i]
		count := 0

		for _, ind := range ctx.population {
			if dominates(p, ind) {
				count++
			}
		}

		p.fit = float64(count) / float64(len(ctx.population)+1)
	}

	for i, _ := range ctx.population {
		p := ctx.population[i]

		for _, nd := range ctx.archive {
			if dominates(nd, p) {
				p.fit += nd.fit
			}
		}

		p.fit += 1
	}

}

func dominates(i Individual, i2 Individual) bool {
	atLeastOne := false
	for it := 0; it < len(i.fIt); it++ {
		if i.fIt[it] > i2.fIt[it] {
			return false
		} else if i.fIt[it] < i2.fIt[it] {
			atLeastOne = true
		}
	}

	return atLeastOne
}

func (ctx *context) selection(population []Individual) []Individual {
	//fitSum := 0.0
	//for _, o := range population {
	//	fitSum += o.fit
	//}
	//
	//cFitness := make([]float64, len(population))
	//cFitness[0] = population[0].fit / fitSum
	//for i := 1; i < ctx.getPopulationSize(); i++ {
	//	cFitness[i] = cFitness[i-1] + (population[i].fit / fitSum)
	//}
	//
	//var newPopulation []Individual
	//for i := 0; i < ctx.getPopulationSize(); i++ {
	//	ind := binary_turnament(population)
	//	newPopulation = append(newPopulation, ind)
	//	rnd := rndGen.Float64()
	//	if rnd < cFitness[0] {
	//		newPopulation = append(newPopulation, population[0])
	//	} else {
	//		for j := range cFitness {
	//			if rnd >= cFitness[j] && rnd < cFitness[j+1] {
	//				newPopulation = append(newPopulation, population[j+1])
	//			}
	//		}
	//	}
	//}

	var newPopulation []Individual
	for i := 0; i < ctx.getPopulationSize(); i++ {
		ind := binaryTurnament(population)
		newPopulation = append(newPopulation, ind)
	}
	return newPopulation
}

func binaryTurnament(population []Individual) Individual {
	i, j := rndGen.RandomInt(len(population)), rndGen.RandomInt(len(population))

	if population[i].fit < population[j].fit {
		return population[i]
	} else {
		return population[j]
	}
}

func (ctx *context) mutation(population []Individual, currentIt int) {
	c := ctx.getMutationChance()

	for i := range population {
		p := population[i]
		for d := 0; d < ctx.getDimensionSize(); d++ {
			rnd := rndGen.Float64()
			if rnd < c {
				maxIt := ctx.getIt()
				x := p.xIt[d]
				var y float64
				if rnd >= 0.5 {
					y = ctx.getMaxForDim(d) - x
				} else {
					y = x - ctx.getMinForDim(d)
				}
				p.xIt[d] = scale(x, y, float64(currentIt), float64(maxIt))
			}
		}
	}

	// dla każdego z p
	// jeżeli trafione
	// wybierz xIt
	// zastosuj wzór ze 160
	// right(k) i left(k) to skrajne wartości domeny
}

func scale(x, y, currentIt, maxIt float64) float64 {
	return x + rndGen.Float64()*(y-x)*(1-float64(currentIt)/float64(maxIt))
}

func newIndividual(x []float64, functions []Function) Individual {
	var fs []float64
	for _, f := range functions {
		val := f.count(x)
		fs = append(fs, val)
	}

	return Individual{
		xIt: x,
		fIt: fs,
	}
}

func selectNotDominated(population []Individual) []Individual { // @todo można skorzystać z drzewa
	var notDominated []Individual

	for _, o := range population {
		isNotDominated := true
		for _, m := range population {
			if dominates(m, o) {
				isNotDominated = false
				break
			}
		}
		if isNotDominated {
			notDominated = append(notDominated, o)
		}
	}
	return notDominated
}
