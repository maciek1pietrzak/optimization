package spea

import rndGen "bitbucket.org/maciek1pietrzak/utils"

func (ctx *context) crossover(population []Individual) {
	c := ctx.getCrossoverChance() / 100
	var crossedPopulation []Individual
	pair := 0
	var mem int
	for i, o := range population {
		if rndGen.Float64() < c {
			pair++
			if pair%2 == 0 {
				o1, o2 := ctx.cross(population, mem, i)
				crossedPopulation = append(crossedPopulation, o1, o2)
			} else {
				mem = i
			}
		} else {
			crossedPopulation = append(crossedPopulation, o)
		}
	}

	ctx.population = crossedPopulation
	// dla każdego z population
	// losuj liczbę
	// jeżeli trafione to wybierz do krzyżowania
	// jeżeli mamy 2 to krzyżujemy
	// może zwrócić 1 lub nic
	// jeżeli zwróci 1 to wymień ze słabszym?
}

// @todo simplify?
func (ctx *context) cross(population []Individual, idx1, idx2 int) (Individual, Individual) {
	o1, o2 := population[idx1], population[idx2]

	if dominates(o1, o2) {
		o3, ok := cross(o1, o2, ctx.getDomains(), ctx.getFunctions())

		if ok {
			return o1, o3
		}
	} else if dominates(o2, o1) {
		o3, ok := cross(o2, o1, ctx.getDomains(), ctx.getFunctions())

		if ok {
			return o1, o3
		}
	}

	return o1, o2
}

func cross(i1, i2 Individual, domains []Domain, funcs []Function) (Individual, bool) {
	var xVec []float64
	for it, d := range domains {
		retry := 0
		for {
			r := rndGen.Float64()
			x := r*(i2.xIt[it]-i1.xIt[it]) + i2.xIt[it]

			if d.minX <= x && d.maxX >= x {
				xVec = append(xVec, x)
				break
			} else if retry == 10 {
				return Individual{}, false
			} else {
				retry++
			}
		}
	}

	return newIndividual(xVec, funcs), true
}
