package spea

type SpeaConfig struct {
	popSize, archSize int
	it                int
	functions         []Function
	domains           []Domain

	mutationChance  float64
	crossoverChange float64
}

type Domain struct {
	minX, maxX float64
}

type Function interface {
	count([]float64) float64
}
