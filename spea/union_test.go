package spea

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestUnion(t *testing.T) {
	k := []int{1, 2, 3}
	l := []int{4, 5, 6}

	m := append(k, l...)

	assert.Equal(t, []int{1, 2, 3}, k)
	assert.Equal(t, []int{4, 5, 6}, l)
	assert.Equal(t, []int{1, 2, 3, 4, 5, 6}, m)
}
