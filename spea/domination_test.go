package spea

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestDomiation(t *testing.T) {
	tests := []struct {
		name     string
		i1, i2   Individual
		expected bool
	}{
		{"should not dominate", newInd([]float64{2, 10}), newInd([]float64{1, 11}), false},
		{"should dominate on one dimension", newInd([]float64{1, 10}), newInd([]float64{1, 11}), true},
		{"should dominate on all dimensions", newInd([]float64{1, 10}), newInd([]float64{2, 11}), true},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, dominates(test.i1, test.i2))
		})
	}
}

func newInd(fIt []float64) Individual {
	return Individual{fIt: fIt}
}
