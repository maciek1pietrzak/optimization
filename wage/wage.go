package wage

import (
	"math"
	"gonum.org/v1/gonum/mat"
)

type extremum struct {
	min, max int
}

func Wage(conf WageConfig) int { // @todo powinno zwracać tablicę
	domainRange := int(math.Abs(float64(conf.xDom.max - conf.xDom.min + 1)))
	critLen := len(conf.criteria)
	critToObj := mat.NewDense(critLen, domainRange, nil)
	for i, c := range conf.criteria {
		extr := extremum{}

		xDenomalize := make([]int, 0, domainRange)
		for x := conf.xDom.min; x <= conf.xDom.max; x++ {
			fx := 0

			fx += c.count(x)
			xDenomalize = append(xDenomalize, fx)

			if extr.min > fx {
				extr.min = fx
			}
			if extr.max < fx {
				extr.max = fx
			}
		}

		xNormalized := normalizeCrit(c, xDenomalize, extr)
		critToObj.SetRow(i, xNormalized)
	}

	objToCrit := critToObj.T()

	normalizedWages := getNormalizedWages(conf)

	xMax := adapation(domainRange, objToCrit, normalizedWages) // @todo można to zoptymalizować przenosząc obliczenia do pętli wyżej

	return xMax
}

func adapation(domainRange int, objToCrit mat.Matrix, normalizedWages []float64) int {
	critNum := len(normalizedWages)
	sMax := float64(math.MinInt64)
	xMax := 0
	for i := 0; i < domainRange; i++ {
		s := 0.0
		for j := 0; j < critNum; j++ {
			s += objToCrit.At(i, j) * normalizedWages[j]
		}

		if sMax < s {
			sMax = s
			xMax = i
		}
	}
	return xMax
}

func getNormalizedWages(conf WageConfig) []float64 {
	denormalizedWages := make([]int, len(conf.criteria))
	for i, crit := range conf.criteria {
		denormalizedWages[i] = crit.wage()
	}
	normalizedWages := normalizeVector(denormalizedWages)
	return normalizedWages
}

func normalizeCrit(c Criterion, xDenomalize []int, extr extremum) []float64 {
	normalizer := criterionTypeToNormalizer[c.criterionType()]
	xNormalized := make([]float64, len(xDenomalize))
	for i, x := range xDenomalize {
		normalized := normalizer.normalize(x, extr.min, extr.max)
		xNormalized[i] = normalized
	}
	return xNormalized
}
