package wage

import (
	"testing"
)

var result int

func BenchmarkPerformance_2Functions(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 10},
		criteria: []Criterion{nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func BenchmarkPerformance_5Functions(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 10},
		criteria: []Criterion{nothing{}, nothing{}, nothing{}, nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func BenchmarkPerformance_10Functions(b *testing.B) {

	c := WageConfig{
		xDom:     Domain{min: 0, max: 10},
		criteria: []Criterion{nothing{}, nothing{}, nothing{}, nothing{}, nothing{}, nothing{}, nothing{}, nothing{}, nothing{}, nothing{}},
	}

	testFunc(c, b.N)
}

func BenchmarkPerformance_10Domain(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 10},
		criteria: []Criterion{nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func BenchmarkPerformance_100Domain(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 100},
		criteria: []Criterion{nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func BenchmarkPerformance_1000Domain(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 1000},
		criteria: []Criterion{nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func BenchmarkPerformance_10000Domain(b *testing.B) {
	c := WageConfig{
		xDom:     Domain{min: 0, max: 10000},
		criteria: []Criterion{nothing{}, nothing{}},
	}
	testFunc(c, b.N)
}

func testFunc(c WageConfig, loop int) {
	var r int
	for i := 0; i < loop; i++ {
		r = Wage(c)
	}
	result = r
}

type nothing struct {
}

func (f nothing) criterionType() CriterionType {
	return STIMULANT
}

func (f nothing) count(x int) int {
	return x
}

func (f nothing) wage() int {
	return 1
}
