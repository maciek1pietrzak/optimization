package wage

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestWageAlgorithm(t *testing.T) {
	testCases := []struct {
		c WageConfig
		x int
	}{
		{
			WageConfig{
				xDom:     Domain{min: 0, max: 10},
				criteria: []Criterion{power{}, negative{}},
			},
			0,
		},
	}

	for _, tc := range testCases {
		x := Wage(tc.c)
		assert.Equal(t, tc.x, x)
	}
}

type power struct {
}

func (f power) criterionType() CriterionType {
	return STIMULANT
}

func (f power) count(x int) int {
	return x * x
}

func (f power) wage() int {
	return 1
}

type negative struct {
}

func (f negative) criterionType() CriterionType {
	return STIMULANT
}

func (f negative) count(x int) int {
	return -x
}

func (f negative) wage() int {
	return 1
}
