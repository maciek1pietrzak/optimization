package wage

import "bitbucket.org/maciek1pietrzak/utils/slice"

type CriterionType int

const (
	STIMULANT   CriterionType = iota
	DESTIMULANT
	//NOMINANT @todo fix
)

var criterionTypeToNormalizer = map[CriterionType]criterionNormalizer{
	STIMULANT:   stimulantNormalizer{},
	DESTIMULANT: destimulatnNormalizer{},
}

type criterionNormalizer interface {
	normalize(x, min, max int) float64
}

type stimulantNormalizer struct {
}

type destimulatnNormalizer struct {
}

type nominantNormalizer struct {
}

func (n stimulantNormalizer) normalize(x, min, max int) float64 {
	i := float64(x - min)
	ref := float64(max - min)
	return i / ref
}

func (n destimulatnNormalizer) normalize(x, min, max int) float64 {
	i := float64(max - x)
	ref := float64(max - min)
	return i / ref
}

func normalizeVector(vector []int) []float64 {
	sum := float64(slice.SumSlice(vector))

	normalized := make([]float64, len(vector))
	for i, w := range vector {
		normalized[i] = float64(w) / sum
	}

	return normalized
}
