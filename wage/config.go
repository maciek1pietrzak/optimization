package wage

type WageConfig struct {
	xDom     Domain // @todo przerobić na slice?
	criteria []Criterion
}

type Criterion interface {
	wage() int
	criterionType() CriterionType
	count(int) int
}

type Domain struct {
	min, max int
}