package wage

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestStimulantSerializer(t *testing.T) {
	// given
	testCases := []struct {
		x, min, max int
		expected    float64
	}{
		{
			1, 0, 1,
			1,
		},
		{
			1, 0, 2,
			0.5,
		},
		{
			1, 1, 2,
			0,
		},
	}
	testee := stimulantNormalizer{}

	for _, tc := range testCases {
		// when
		x := testee.normalize(tc.x, tc.min, tc.max)

		// then
		assert.Equal(t, tc.expected, x)
	}
}

func TestDestimulantSerializer(t *testing.T) {
	// given
	testCases := []struct {
		x, min, max int
		expected    float64
	}{
		{
			1, 0, 1,
			0,
		},
		{
			1, 0, 2,
			0.5,
		},
		{
			1, 1, 2,
			1,
		},
	}
	testee := destimulatnNormalizer{}

	for _, tc := range testCases {
		// when
		x := testee.normalize(tc.x, tc.min, tc.max)

		// then
		assert.Equal(t, tc.expected, x)
	}
}

func TestVectorNormalization(t *testing.T) {
	testCases := []struct {
		x []int
		expected    []float64
	}{
		{
			[]int{10, 10},
			[]float64{0.5, 0.5},
		},
		{
			[]int{1, 2, 2, 5},
			[]float64{.1, .2, .2, .5},
		},
	}

	for _, tc := range testCases {
		// when
		w := normalizeVector(tc.x)

		// then
		assert.Equal(t, tc.expected, w)
	}

}